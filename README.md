# API_Demo


## INTRODUCCIÓN

El presente documento contiene la descripción de la instalación de la API. Donde se puede visualizar cómo iniciar y para la aplicación, los logs y el sitio donde se encuentra la API. 

 

## DIRECTORIO

Los microservicios se encuentran desplegados en el directorio /opt/wso2/microservices . El presente microservicio se encuentra en los siguientes servidores y directorios:

*  Servidores donde esta instalado : 
    *   **QA** : -
    *   **PROD** :  -
   
  * Datos comunes:  
    *   **Usuario SSH**: esb
    *   **Directorio** del microservicio: 
    
    `/opt/wso2/microservices/API_Demo`
          

## PRE REQUISITOS PARA INICIAR LA APLICACIÓN 

El servidor de destino debe contar con los siguientes ítems:
*   En el sistema operativo de destino 
    *   docker 
    *   docker-compose 
    *   Crear red docker con el nombre "wso2". En esta red estará desplegado el Wso2 Analytics facilitando así la comunicación con el mismo. Para crear la red ejecutar el siguiente comando:
    *   <code><em>docker network create -d bridge wso2</em></code>
 
 
## INICIAR APLICACIÓN

Para iniciar la aplicación debemos dirigirnos a la carpeta de la api 

`cd /opt/wso2/microservices/API_Demo`

Ejecutar el comando

`docker-compose up -d`

Verificar si está corriendo 

`docker ps -f name=demo_api_demo_api_1`


## PARAR APLICACIÓN

Para iniciar la aplicación debemos dirigirnos a la carpeta de la api 

`cd /opt/wso2/microservices/API_Demo`

Ejecutar el comando

`docker stop demo_api_demo_api_1`

Verificar si está corriendo (no debería aparecer)

`docker ps -f name=demo_api_demo_api_1`


## REVISAR LOGS

Para revisar los logs de la aplicación nos dirigimos a la carpeta logs de la aplicación.
 
`docker logs -f --tail 500 demo_api_demo_api_1`


## Puertos

Los puertos del contenedor estan definidos en el balanceador **Traefik** mediante los _labels_  incorporados en el archivo **docker-compose.yml**:

**HTTP:** 8280

**HTTPS:** 8243

## SWAGGER DE LA API

El swagger de la API es la definición que establece las url y las propiedades necesarias para la ejecución de una API. 

Para obtener el swagger se debe ejecutar: 

http://&lt;server_url>:&lt;port>/&lt;API_CONTEXT>/swagger.yaml

Donde &lt;port> son los puertos del balanceador **Traefik** a los docker.

En este caso el  &lt;API_CONTEXT> es /demo

URL del Swagger: 

`http://localhost:8280/demo/swagger.yaml`


## HEALTH CHECK

La operación de health check permite comprobar el estatus de la api. 

La url correspondiente a la operación health check está construida de la siguiente forma: 

http://&lt;server_url>:&lt;port>/&lt;API_CONTEXT>/health/status

En este caso el  &lt;API_CONTEXT> es /demo

En este caso la url es la siguiente:

`http://localhost:8280/demo/health/status`



## ESTRUCTURA DE CARPETA 

La carpeta de la API tiene una estructura de carpetas que depende de los volúmenes montados en el archivo docker-compose.yml. Así como, las plantillas para aplicar los valores de configuración documentados en el archivo env.json. 

Las carpetas son:

*   **carbonapps:** donde se copian los archivos .car que contienen las aplicaciones a desplegar en el microintegrator.   ** **
*   **conf**: donde se encuentra el archivo log4j2.properties para ajustar el nivel de debug de los distintos logs y el archivo deployment.toml generado a partir de la plantilla templates/deployment.toml.j2 (al momento de ejecutar el script deploy.sh) 
*   **logs:** donde se crean los diferentes archivos de logs al momento de la ejecución del micro integrator. Por ejemplo el archivo wso2carbon.log 
*   **registry:** donde se montan los distintos archivos registry, ya sea desplegados por medios del car, o desplegados mediante la creación de la estructura correspondiente a la definición establecida en el código. 
*   **templates:** donde se encuentran los archivos j2 que son ejecutados en base al archivo env.json y los comandos descritos en el archivo deploy.sh. 
    *   docker-compose.yml.j2 construye el archivo docker-compose.yml utilizando las variables de ambiente descritas en el archivo env.json. Notar que en este archivo se describen las variables de ambiente del sistema operativo que luego el código synapse carga al momento de ejecución. 
    *   deployment.toml.j2 construye el archivo deployment.toml utilizando las variables de ambiente descritas en el archivo env.json. Contiene los parámetros de configuración del micro integrator 


Ejemplo de estructura de carpetas


```
.
|-- carbonapps
|-- conf
|   |-- deployment.toml
|   `-- log4j2.properties
|-- logs
|-- registry
|-- templates
|-- env.json	
|-- docker-compose.yml	

```


**Notar**  que los puertos de exposición del docker estan definidos en el balanceador de carga segun arquitectura de referencia : [link](https://wiki-desarrollo.ccu.cl/books/docker-9a9/page/arquitectura-docker) . 

## UTILIDADES

Iniciar Repositorio

`git init`

`git add .`

`git commit -a -m  "commit inicial"`

`git push --set-upstream '<url_repo>' master`

`git remote add origin '<url_repo>'`

Subir un cambio

`git commit -a -m "commit inicial" && git push -u origin HEAD`